module gitlab.com/patipol.thamdee/coactor

go 1.15

require (
	github.com/briandowns/spinner v1.12.0 // indirect
	github.com/cavaliercoder/grab v2.0.0+incompatible // indirect
	github.com/cheggaaa/pb/v3 v3.0.5 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/gocolly/colly/v2 v2.1.0
	github.com/google/uuid v1.2.0 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/logrusorgru/aurora/v3 v3.0.0 // indirect
	github.com/whiteshtef/clockwork v0.0.0-20200221012748-027e62affd84
)
