package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"

	"github.com/cheggaaa/pb/v3"
)

// var contentType *string
// var userName *string
// var password *string
var url *string
var count int
var bar *pb.ProgressBar

// userName := flag.String("u", "", "")
// password := flag.String("p", "", "")

func main() {
	// contentType = flag.String("t", "twitch", "")
	// userName = flag.String("u", "", "")
	// password = flag.String("p", "", "")
	url = flag.String("u", "", "")

	flag.Parse()

	downloadFile(".", *url)
}

// func colorPrint() {
// 	fmt.Println(*contentType)
// 	fmt.Println(*userName)
// 	fmt.Println(*password)

// 	fmt.Println(aurora.Bold(aurora.Cyan(*contentType)))
// 	fmt.Println(aurora.Bold(aurora.Cyan(*userName)))
// 	fmt.Println(aurora.Bold(aurora.Cyan(*password)))

// }

func printDownloadPercent(done chan int64, path string, total int64) {
	var stop bool = false

	for {
		select {
		case <-done:
			stop = true
		default:

			file, err := os.Open(path)
			if err != nil {
				log.Fatal(err)
			}

			fi, err := file.Stat()
			if err != nil {
				log.Fatal(err)
			}

			size := fi.Size()

			if size == 0 {
				size = 1
			}

			if count == 0 {
				bar = pb.StartNew(int(total))
				count++
			}
			bar.SetCurrent(size)

		}

		if stop {
			bar.SetCurrent(total)
			bar.Finish()
			break

		}

		time.Sleep(time.Second)
	}

}

func downloadFile(dest string, url string) {

	file := path.Base(url)

	log.Printf("Downloading file %s from %s\n", file, url)

	var path bytes.Buffer
	path.WriteString(dest)
	path.WriteString("/")
	path.WriteString(file)

	start := time.Now()

	out, err := os.Create(path.String())

	if err != nil {
		fmt.Println(path.String())
		panic(err)
	}

	defer out.Close()

	headResp, err := http.Head(url)

	if err != nil {
		panic(err)
	}

	defer headResp.Body.Close()

	size, err := strconv.Atoi(headResp.Header.Get("Content-Length"))

	if err != nil {
		panic(err)
	}

	done := make(chan int64)

	go printDownloadPercent(done, path.String(), int64(size))

	resp, err := http.Get(url)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	n, err := io.Copy(out, resp.Body)

	if err != nil {
		panic(err)
	}

	done <- n

	elapsed := time.Since(start)
	fmt.Println("")
	log.Printf("Download completed in %s", elapsed)
}
